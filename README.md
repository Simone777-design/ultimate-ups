# Ultimate UPS

An Uninterrupted Power Supply that acts as a back-up battery when there is a power outage/interruption.

The Uninterrupted Power supply operates using 3 subsystems: the voltage constrainer, the voltage regulator and status LEDs. The voltage constrainer constrains the voltage to 0-3.3V (the voltages at which the PiHat operates at). The power regulator outputs a constant 5V regardless of the varying input of the back-up battery. The status LEDs monitor and displays how full the battery is.

The UPS can be used in household devices to ensure that these devices continue to function for approximately 10-15min after the power outage/interruption has occurred.
